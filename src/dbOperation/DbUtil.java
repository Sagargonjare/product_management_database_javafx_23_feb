package dbOperation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {
	static Statement stmt;
	static Connection con;
	public static void createDbConnection () {
		 try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/product_ManagementJavaFx", "root", "@Sagar05");
		     stmt =con.createStatement();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public static void executeQuery(String query) {
		 try {
			stmt.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
