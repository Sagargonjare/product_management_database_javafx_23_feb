package commonScrenn;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import stageMaster.StageMaster;

public class CommonScreen {
	public void show()  {
		try {
			Parent actorGroup =FXMLLoader.load(getClass().getResource(getClass().getSimpleName()+".fxml"));
			StageMaster.getstage().setScene(new Scene(actorGroup));
			StageMaster.getstage().show();
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}

}
